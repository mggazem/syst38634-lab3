package lab3Part2;
/**
 * @author Malek Gazem
 * Date: 22nd Jan, 2020
 * Program: Validates passwords if they are more than 8 characters and containing two or more integers
 */
public class PasswordValidater {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String pass = "asd123f";
		checkPasswordLength(pass);
		checkDigits(pass);
	}
	/**
	 * @param pass
	 * checks if the password is 8 characters or long
	 * @return
	 */
	public static boolean checkPasswordLength(String pass) {
		//checks if the password is 8 characters or long
		if(pass.length() >=8)
			return true;
		else
			return false;
	}
	/**
	 * @param pass
	 * Checks if the password has two or more digits
	 * @return
	 */
	public static boolean checkDigits(String pass) {
		//after refactoring. Realized the above method only checked if it had an integer, and did not satisfy the 2nd requirement. It failed the boundary out. Thus refactored it to this.
		int count =0 ; // 
		String test;
		//Goes through the whole string
		for(int i=0; i<pass.length(); i++) {
			test = pass.substring(i, i+1) ;//Separates string into char
			if((test.matches(".*\\d.*")))//compares it with the integer regular expression
				count++;
		}
		System.out.println("Count: " + count);
		if(count>=2)
			return true;
		
		return false;	
	}

	
}
