package lab3Part2;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PasswordValidaterTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCheckPasswordresult() {
		boolean result = PasswordValidater.checkPasswordLength("abcdefgh");
		System.out.println("Regular:  " + result);
		assertTrue("The password does not have 8 characters", result == true);
	}
	@Test
	public void boundaryInTestCheckPasswordresult() {
		boolean result = PasswordValidater.checkPasswordLength("abcdefgh");
		System.out.println("Boundary In result:  " + result);
		assertTrue("The password does not have 8 characters", result == true);
	}
	@Test
	public void boundaryOutTestCheckPasswordresult() {
		boolean result = PasswordValidater.checkPasswordLength("abcdefg");
		System.out.println("Boundary Out result:  " + result);
		assertTrue("The password does not have 8 characters", result == false);
	}
	@Test
	public void exceptionTestCheckPasswordresult() {
		boolean result = PasswordValidater.checkPasswordLength("");
		System.out.println("Exception result:  " + result);
		assertTrue("The password does not have 8 characters", result == false);
	}

	@Test
	public void testCheckDigits() {
		boolean result = PasswordValidater.checkDigits("abcdefgh123");
		System.out.println("Regular:  " + result);
		assertTrue("The password does not contain two digits", result == true);
	}
	@Test
	public void exceptionTestCheckDigits() {
		boolean result = PasswordValidater.checkDigits("abcdefgh");
		System.out.println("Exception:  " + result);
		assertTrue("The password does not contain two digits", result == false);
	}
	@Test
	public void boundaryInTestCheckDigits() {
		boolean result = PasswordValidater.checkDigits("abcdefgh12");
		System.out.println("Boundary In:  " + result);
		assertTrue("The password does not contain two digits", result == true);
	}
	@Test
	public void boundaryOutTestCheckDigits() {
		boolean result = PasswordValidater.checkDigits("abcdefgh1");
		System.out.println("Boundary Out:  " + result);
		assertTrue("The password does not contain two digits", result == false);
	}

}
